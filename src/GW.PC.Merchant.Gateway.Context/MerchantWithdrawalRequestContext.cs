﻿using Newtonsoft.Json;
using System.Linq;

namespace GW.PC.Merchant.Gateway.Context
{
    public class MerchantWithdrawalRequestContext : MerchantRequestContext
    {
        /// <summary>
        /// 支付类型选择网银、在线支付需提供，至 支付中心后台 设置对应支付商银行代码
        /// </summary>
        [JsonProperty("bankCode")]
        public string BankCode { get; set; }       
        /// <summary>
        /// 收款卡号
        /// </summary>
        [JsonProperty("payeeCardNumber")]
        public string PayeeCardNumber { get; set; }
        /// <summary>
        /// 收款人姓名
        /// </summary>
        [JsonProperty("payeeName")]
        public string PayeeName { get; set; }


        public override string ValidateGeneralInfo()
        {
            var errorArray = GetType().GetProperties()
                .Where(w => w.GetValue(this) == null || w.GetValue(this).ToString().Length == 0)
                .Select(s => s.CustomAttributes.FirstOrDefault(x => x.AttributeType == typeof(JsonPropertyAttribute)))
                .ToArray().Select(s => s.ConstructorArguments.SingleOrDefault().Value).ToArray();

            return string.Join(",", errorArray);
        }
    }
}
