﻿using GW.PC.Payment.SDK;
using Newtonsoft.Json;

namespace GW.PC.Merchant.Gateway.Context
{
    public class MerchantQueryRequestContext : MerchantRequestContext
    {
        /// <summary>
        /// 支付方式
        /// </summary>
        [JsonProperty("payMethod")]
        public string PaymentMethod { get; set; }
    }
}
