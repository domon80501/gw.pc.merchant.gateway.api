﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Merchant.Gateway.Context
{
    public enum RequestType
    {
        Deposit = 1,
        Withdrawal = 2,
        DepositQuery = 3,
        WithdrawalQuery = 4,
        Callback = 5
    }
}
