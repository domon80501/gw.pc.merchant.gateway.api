﻿using GW.PC.Payment.SDK;
using Newtonsoft.Json;
using GW.PC.Core;

namespace GW.PC.Merchant.Gateway.Context
{
    public class MerchantResponseContext: MerchantSharedContext
    {
        /// <summary>
        /// 商户订单号
        /// </summary>
        [JsonProperty("merchantOrderNo")]
        public string MerchantOrderNumber { get; set; }
        /// <summary>
        /// 支付中心订单号
        /// </summary>
        [JsonProperty("systemOrderNo")]
        public string PaymentCenterOrderNumber { get; set; }
        /// <summary>
        /// 支付商订单号
        /// </summary>
        [JsonProperty("paymentOrderNo")]
        public string PaymentOrderNumber { get; set; }
        /// <summary>
        /// 支付网址
        /// </summary>
        [JsonProperty("payContent")]
        public string Content { get; set; }
        /// <summary>
        /// 交易行为状态
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }        
        /// <summary>
        /// 签名
        /// </summary>
        [SignIgnore]
        [JsonProperty("sign")]
        public string Sign { get; set; }
    }
}
