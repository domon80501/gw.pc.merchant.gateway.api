﻿using GW.PC.Payment.SDK;
using Newtonsoft.Json;

namespace GW.PC.Merchant.Gateway.Context
{
    public class MerchantSharedContext
    {
        /// <summary>
        /// 商户与支付中心的请求状态
        /// </summary>
        [JsonProperty("reqStatus")]
        public string RequestStatus { get; set; }
        [JsonProperty("noticeType")]
        public string NoticeType { get; set; }
        /// <summary>
        /// 错误讯息
        /// </summary>
        [JsonProperty("errorMsg")]
        public string Message { get; set; }
    }
}
