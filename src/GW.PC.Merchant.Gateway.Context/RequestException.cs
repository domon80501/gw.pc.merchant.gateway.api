﻿using System;
using System.Net;

namespace GW.PC.Merchant.Gateway.Context
{
    /// <summary>
    /// Represents the exceptions supposed to show the message to users.
    /// </summary>
    public class RequestException : Exception
    {
        public RequestException(string message, NoticeType noticeType, HttpStatusCode httpStatusCode = HttpStatusCode.BadRequest, RequestStatus requestStatus = RequestStatus.Fail) : base(message)
        {
            NoticeType = noticeType;
            HttpStatusCode = httpStatusCode;
            RequestStatus = requestStatus;
        }

        public NoticeType NoticeType { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
        public RequestStatus RequestStatus { get; set; }
    }
}
