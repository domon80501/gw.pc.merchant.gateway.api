﻿using GW.PC.Payment.SDK;
using Newtonsoft.Json;

namespace GW.PC.Merchant.Gateway.Context
{
    public class MerchantQueryResponseContext : MerchantResponseContext
    {
        [JsonProperty("reqAmount")]
        public string RequestAmount { get; set; }
        /// <summary>
        /// 实际金额
        /// </summary>
        [JsonProperty("amount")]
        public string ActualAmount { get; set; }
    }
}
