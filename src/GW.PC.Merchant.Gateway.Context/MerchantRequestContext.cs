﻿using GW.PC.Payment.SDK;
using Newtonsoft.Json;
using System.Linq;

namespace GW.PC.Merchant.Gateway.Context
{
    public class MerchantRequestContext
    {
        /// <summary>
        /// 商户号
        /// </summary>
        [JsonProperty("merchantCode")]
        public string MerchantUsername { get; set; }
        /// <summary>
        /// 商户订单号
        /// </summary>
        [JsonProperty("merchantOrderNo")]
        public string MerchantOrderNumber { get; set; }
        /// <summary>
        /// 请求金额
        /// </summary>
        [JsonProperty("reqAmount")]
        public string RequestedAmount { get; set; }
        /// <summary>
        /// 商户之客户端请求识别号(客户帐号)
        /// </summary>
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 客户端请求ip
        /// </summary>
        [JsonProperty("ip")]
        public string Ip { get; set; }
        [JsonProperty("plat")]
        public string PaymentPlatform { get; set; }
        /// <summary>
        /// 真钱交易成功后，回调的目的地址
        /// </summary>
        [JsonProperty("callbackUrl")]
        public string CallbackUrl { get; set; }        

        [SignIgnore]
        [JsonProperty("sign")]
        public string Sign { get; set; }


        /// <summary>
        /// 验证商户端基础参数是否填写
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public virtual string ValidateGeneralInfo()
        {
            var jsonString = JsonConvert.SerializeObject(this);
            var generalContext = JsonConvert.DeserializeObject<MerchantRequestContext>(jsonString);

            var errorArray = generalContext.GetType().GetProperties()
                .Where(w => w.GetValue(this) == null || w.GetValue(this).ToString().Length == 0)
                .Select(s => s.CustomAttributes.FirstOrDefault(x => x.AttributeType == typeof(JsonPropertyAttribute)))
                .ToArray().Select(s => s.ConstructorArguments.SingleOrDefault().Value).ToArray();

            return string.Join(",", errorArray);
        }
    }
}
