﻿using GW.PC.Payment.SDK;
using Newtonsoft.Json;

namespace GW.PC.Merchant.Gateway.Context
{
    public class MerchantDepositRequestContext : MerchantRequestContext
    {
        /// <summary>
        /// 支付方式
        /// </summary>
        [JsonProperty("payMethod")]
        public string PaymentMethod { get; set; }        
        /// <summary>
        /// 支付类型选择网银、在线支付需提供，至 支付中心后台 设置对应支付商银行代码
        /// </summary>
        [JsonProperty("bankCode")]
        public string BankCode { get; set; }
        /// <summary>
        /// 付款人姓名
        /// </summary>
        [JsonProperty("payerName")]
        public string PayerName { get; set; }
        /// <summary>
        /// 充值卡号
        /// </summary>
        [JsonProperty("cardSN")]
        public string CardSerialNumber { get; set; }
        /// <summary>
        /// 充值卡密码
        /// </summary>
        [JsonProperty("cardPwd")]
        public string CardPassword { get; set; }
        /// <summary>
        /// 付款成功后的转页(重定向)网址
        /// </summary>
        [JsonProperty("returnUrl")]
        public string RedirectionUrl { get; set; }
    }
}
