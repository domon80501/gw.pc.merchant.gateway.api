﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Merchant.Gateway.Context
{
    public enum NoticeType
    {   
        PaymentCenter = 1,
        PaymentProvider = 2
    }
}
