﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace GW.PC.Transfer.Gateway.Api
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            // 應用程式啟動時執行的程式碼            
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}