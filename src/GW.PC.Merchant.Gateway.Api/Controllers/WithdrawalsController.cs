﻿using GW.PC.Core;
using GW.PC.Merchant.Gateway.Context;
using GW.PC.Payment.Api.Client;
using GW.PC.Web.Core;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace GW.PC.Merchant.Gateway.Api.Controllers
{
    public class WithdrawalsController : ApiController
    {
        public async Task<HttpResponseMessage> Get()
        {
            var guid = Guid.NewGuid().ToString();

            Request.Properties.Add("guid", guid);

            var requestContext = WebUtility.DeserializeQueryString<MerchantQueryRequestContext>(Request.RequestUri.Query.Substring(1));
            requestContext.Sign = HttpUtility.UrlDecode(requestContext.Sign);

            LogHelper.LogMerchantQueryRequestReceived(guid, JsonConvert.SerializeObject(requestContext));

            ValidateGetInfo(requestContext);

            var result = await new WithdrawalsClient().Query(requestContext);

            LogHelper.LogMerchantQueryResponded(guid, JsonConvert.SerializeObject(result));

            return result;
        }

        public async Task<HttpResponseMessage> Post(MerchantWithdrawalRequestContext requestContext)
        {
            var guid = Guid.NewGuid().ToString();

            Request.Properties.Add("guid", guid);

            LogHelper.LogMerchantWithdrawalRequestReceived(guid, JsonConvert.SerializeObject(requestContext));

            ValidatePostInfo(requestContext);

            var result = await new WithdrawalsClient().Request(requestContext);

            LogHelper.LogMerchantWithdrawalResponded(guid, JsonConvert.SerializeObject(result));

            return result;
        }

        private void ValidateGetInfo(MerchantQueryRequestContext context)
        {
            if (string.IsNullOrEmpty(context.MerchantUsername))
            {
                throw new RequestException(string.Format(Constants.MessageTemplates.InvalidParameters, "merchantCode"), NoticeType.PaymentCenter);
            }

            if (string.IsNullOrEmpty(context.MerchantOrderNumber))
            {
                throw new RequestException(string.Format(Constants.MessageTemplates.InvalidParameters, "merchantOrderNo"), NoticeType.PaymentCenter);
            }
        }

        private void ValidatePostInfo(MerchantWithdrawalRequestContext context)
        {
            var errors = context.ValidateGeneralInfo();

            if (errors.Length > 0)
            {
                throw new RequestException(string.Format(Constants.MessageTemplates.InvalidParameters, errors), NoticeType.PaymentCenter);
            }
        }
    }
}