﻿using GW.PC.Core;
using GW.PC.Merchant.Gateway.Context;
using GW.PC.Payment.Api.Client;
using GW.PC.Web.Core;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace GW.PC.Merchant.Gateway.Api.Controllers
{
    public class DepositsController : ApiController
    {
        public async Task<HttpResponseMessage> Get()
        {
            var guid = Guid.NewGuid().ToString();

            Request.Properties.Add("guid", guid);

            var requestContext = WebUtility.DeserializeQueryString<MerchantQueryRequestContext>(Request.RequestUri.Query.Substring(1));
            requestContext.Sign = HttpUtility.UrlDecode(requestContext.Sign);

            LogHelper.LogMerchantQueryRequestReceived(guid, JsonConvert.SerializeObject(requestContext));

            ValidateGetInfo(requestContext);

            var result = await new DepositsClient().Query(requestContext);

            LogHelper.LogMerchantQueryResponded(guid, JsonConvert.SerializeObject(result));

            return result;
        }

        public async Task<HttpResponseMessage> Post(MerchantDepositRequestContext requestContext)
        {
            var guid = Guid.NewGuid().ToString();

            Request.Properties.Add("guid", guid);

            LogHelper.LogMerchantDepositRequestReceived(guid, JsonConvert.SerializeObject(requestContext));

            ValidatePostInfo(requestContext);

            var result = await new DepositsClient().Request(requestContext);

            LogHelper.LogMerchantDepositResponded(guid, JsonConvert.SerializeObject(result));

            return result;
        }

        private void ValidateGetInfo(MerchantQueryRequestContext context)
        {
            if (string.IsNullOrEmpty(context.MerchantUsername))
            {
                throw new RequestException(string.Format(Constants.MessageTemplates.InvalidParameters, "merchantCode"), NoticeType.PaymentCenter);
            }

            if (string.IsNullOrEmpty(context.MerchantOrderNumber))
            {
                throw new RequestException(string.Format(Constants.MessageTemplates.InvalidParameters, "merchantOrderNo"), NoticeType.PaymentCenter);
            }

            if (string.IsNullOrEmpty(context.PaymentMethod) || !Enum.IsDefined(typeof(PaymentMethod), Convert.ToInt32(context.PaymentMethod)))
            {
                throw new RequestException(string.Format(Constants.MessageTemplates.InvalidParameters, "payMethod"), NoticeType.PaymentCenter);
            }
        }

        private void ValidatePostInfo(MerchantDepositRequestContext context)
        {
            var errors = context.ValidateGeneralInfo();

            if (errors.Length > 0)
            {
                throw new RequestException(string.Format(Constants.MessageTemplates.InvalidParameters, errors), NoticeType.PaymentCenter);
            }

            if (string.IsNullOrEmpty(context.RedirectionUrl))
            {
                throw new RequestException(string.Format(Constants.MessageTemplates.InvalidParameters, "returnUrl"), NoticeType.PaymentCenter);
            }

            if (!Enum.IsDefined(typeof(PaymentMethod), Convert.ToInt32(context.PaymentMethod)))
            {
                throw new RequestException(string.Format(Constants.MessageTemplates.InvalidParameters, "payMethod"), NoticeType.PaymentCenter);
            }

            if (Enum.TryParse(context.PaymentMethod, out PaymentMethod paymentMethod))
            {
                switch (paymentMethod)
                {
                    case PaymentMethod.OnlinePayment:
                    case PaymentMethod.OnlineBanking:
                        if (string.IsNullOrEmpty(context.BankCode))
                        {
                            throw new RequestException(string.Format(Constants.MessageTemplates.InvalidParameters, "bankCode"), NoticeType.PaymentCenter);
                        }
                        break;

                    case PaymentMethod.PrepaidCard:
                        if (string.IsNullOrEmpty(context.CardSerialNumber) || string.IsNullOrEmpty(context.CardPassword))
                        {
                            throw new RequestException(string.Format(Constants.MessageTemplates.InvalidParameters, "cardSN,cardSP"), NoticeType.PaymentCenter);
                        }
                        break;

                    case PaymentMethod.OnlineToBankCard:
                        if (string.IsNullOrEmpty(context.PayerName))
                        {
                            throw new RequestException(string.Format(Constants.MessageTemplates.InvalidParameters, "payerName"), NoticeType.PaymentCenter);
                        }
                        break;
                }
            }

        }
    }
}