﻿using GW.PC.Core;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GW.PC.Merchant.Gateway.Api.Controllers
{
    [RoutePrefix("tsg")]
    public class TransferStationGatewayController : Controller
    {
        [Route("request")]
        public new async Task<ActionResult> Request()
        {
            try
            {
                var body = string.Empty;
                using (var sr = new StreamReader(base.Request.InputStream))
                {
                    body = await sr.ReadToEndAsync();
                }
                SerilogLogger.Logger.LogInformation($"POST request received: body={body}");

                return new RedirectAndPostActionResult(base.Request.Form["Url"], Server.UrlDecode(base.Request.Form["Content"]));
            }
            catch (Exception ex)
            {
                SerilogLogger.Logger.LogInformation(ex.Message);
                return Content(ex.Message);
            }
        }

        [HttpGet]
        [Route("requestget")]
        public new ActionResult Request(string url)
        {
            try
            {
                SerilogLogger.Logger.LogInformation($"GET request received: url={url}");

                ViewBag.Url = url;

                return View();
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        [HttpGet]
        [Route("requestfromhtml")]
        public ActionResult RequestFromHtml()
        {
            try
            {
                var url = base.Request.Url.Query;

                SerilogLogger.Logger.LogInformation($"GET request received: url={url}");

                var item = url.Replace("?content=", "");
                var html = Encoding.UTF8.GetString(Convert.FromBase64String(item));

                return new RedirectAndPostActionResult(html);
            }
            catch (Exception ex)
            {
                SerilogLogger.Logger.LogInformation(ex.Message);
                return Content(ex.Message);
            }
        }

        [HttpGet]
        [Route("requestfromurl")]
        public ActionResult RequestFromUrl()
        {
            try
            {
                var url = base.Request.Url.Query;
                SerilogLogger.Logger.LogInformation($"GET request received: url={url}");

                var items = HttpUtility.ParseQueryString(url);
                var dic = new Dictionary<string, object>();
                for (int i = 0; i < items.Count; i++)
                {
                    dic.Add(items.Keys[i], items[i]);
                }

                return new RedirectAndPostActionResult(items["url"], dic);
            }
            catch (Exception ex)
            {
                SerilogLogger.Logger.LogInformation(ex.Message);
                return Content(ex.Message);
            }
        }

        /// <summary>
        /// 接收參數 url為 alipay://  raybet:// ，替app装置中转
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("requestfromwap")]
        public ActionResult RequestFromWap()
        {
            try
            {
                var url = base.Request.Url.Query;

                SerilogLogger.Logger.LogInformation($"GET requestfromwap received: url={url}");

                var destination = url.Replace("?url=", "");

                return Redirect(destination);
            }
            catch (Exception ex)
            {
                SerilogLogger.Logger.LogInformation(ex.Message);
                return Content(ex.Message);
            }
        }
    }
}