﻿using GW.PC.Core;
using GW.PC.Merchant.Gateway.Context;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http.Filters;

namespace GW.PC.Merchant.Gateway.Api
{
    public class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            string guid = string.Empty;
            if (actionExecutedContext.Request.Properties.TryGetValue("guid", out object tmp))
            {
                guid = tmp.ToString();
            }

            var baseException = actionExecutedContext.Exception.GetBaseException();
            
            var result = new MerchantSharedContext
            {
                RequestStatus = RequestStatus.Fail.ToInt32String(),
                NoticeType = GetNoticeType(baseException).ToInt32String(),
                Message = baseException.Message
            };

            SerilogLogger.Logger.LogError($"{baseException.GetType().Name} - {guid}: {JsonConvert.SerializeObject(result)}");

            var responseMessage = actionExecutedContext.Request.CreateResponse(
                GetHttpStatusCode(baseException),
                result,
                JsonMediaTypeFormatter.DefaultMediaType);
            responseMessage.Headers.Add("Access-Control-Allow-Origin", "*");

            actionExecutedContext.Response = responseMessage;
        }

        private NoticeType GetNoticeType(Exception exception)
        {
            if (exception is RequestException re)
            {
                return re.NoticeType;
            }

            return NoticeType.PaymentCenter;
        }

        private HttpStatusCode GetHttpStatusCode(Exception ex)
        {
            if (ex is RequestException re)
            {
                return re.HttpStatusCode;
            }

            return HttpStatusCode.InternalServerError;
        }
    }
}
