﻿using GW.PC.Merchant.Gateway.Context;
using System.Net.Http;
using System.Threading.Tasks;

namespace GW.PC.Merchant.Gateway.Api.Client
{
    public class MerchantDepositsGatewayClient : MerchantDepositsGatewayBaseClient
    {
        public MerchantDepositsGatewayClient()
        {
            RoutePrefix = "api";
        }

        public Task<HttpResponseMessage> Query(MerchantQueryRequestContext context)
        {
            return GetAsync("deposits", context);
        }
        public Task<HttpResponseMessage> Request(MerchantDepositRequestContext context)
        {   
            return PostAsync("deposits", context);
        }
    }
}
