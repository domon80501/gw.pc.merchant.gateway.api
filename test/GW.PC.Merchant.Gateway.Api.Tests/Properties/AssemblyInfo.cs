using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("GW.PC.Merchant.Gateway.Api.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SYNNEX")]
[assembly: AssemblyProduct("GW.PC.Merchant.Gateway.Api.Tests")]
[assembly: AssemblyCopyright("Copyright © SYNNEX 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("581ed71a-c843-40c1-ae4b-0fa5f9fcd8cb")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
