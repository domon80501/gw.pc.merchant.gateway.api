﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GW.PC.Merchant.Gateway.Context;
using System.Linq;
using GW.PC.Payment.SDK;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using GW.PC.Merchant.Gateway.Api.Client;
using System.Threading.Tasks;
using Newtonsoft.Json;
using GW.PC.Core;
using System.Web;

namespace GW.PC.Merchant.Gateway.Api.Tests
{
    [TestClass]
    public class Deposits
    {
        string merchantOrderNumber = "";
        [TestMethod]
        public async Task RequestWechat_Success()
        {
            var request = CreateReqeust(PaymentMethod.WeChat);

            var response = await new MerchantDepositsGatewayClient().Request(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        [TestMethod]
        public async Task QueryWechat_Success()
        {
            await RequestWechat_Success();
            var request = CreateQueryReqeust(PaymentMethod.WeChat);

            var response = await new MerchantDepositsGatewayClient().Query(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        [TestMethod]
        public async Task RequestAlipay_Success()
        {
            var request = CreateReqeust(PaymentMethod.Alipay);

            var response = await new MerchantDepositsGatewayClient().Request(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        [TestMethod]
        public async Task QueryAlipay_Success()
        {
            await RequestAlipay_Success();
            var request = CreateQueryReqeust(PaymentMethod.Alipay);

            var response = await new MerchantDepositsGatewayClient().Query(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        [TestMethod]
        public async Task RequestOnlinePayment_Success()
        {
            var request = CreateReqeust(PaymentMethod.OnlinePayment);

            var response = await new MerchantDepositsGatewayClient().Request(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        [TestMethod]
        public async Task QueryOnlinePayment_Success()
        {
            await RequestOnlinePayment_Success();
            var request = CreateQueryReqeust(PaymentMethod.OnlinePayment);

            var response = await new MerchantDepositsGatewayClient().Query(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        [TestMethod]
        public async Task RequestOnlineBanking_Success()
        {
            var request = CreateReqeust(PaymentMethod.OnlineBanking);

            var response = await new MerchantDepositsGatewayClient().Request(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        [TestMethod]
        public async Task QueryOnlineBanking_Success()
        {
            await RequestOnlineBanking_Success();
            var request = CreateQueryReqeust(PaymentMethod.OnlineBanking);

            var response = await new MerchantDepositsGatewayClient().Query(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        [TestMethod]
        public async Task RequestPrepaidCard_Success()
        {
            var request = CreateReqeust(PaymentMethod.PrepaidCard);

            var response = await new MerchantDepositsGatewayClient().Request(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        public async Task QueryPrepaidCard_Success()
        {
            await RequestPrepaidCard_Success();
            var request = CreateQueryReqeust(PaymentMethod.PrepaidCard);

            var response = await new MerchantDepositsGatewayClient().Query(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        [TestMethod]
        public async Task RequestQQWallet_Success()
        {
            var request = CreateReqeust(PaymentMethod.QQWallet);

            var response = await new MerchantDepositsGatewayClient().Request(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        public async Task QueryQQWallet_Success()
        {
            await RequestQQWallet_Success();
            var request = CreateQueryReqeust(PaymentMethod.QQWallet);

            var response = await new MerchantDepositsGatewayClient().Query(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        [TestMethod]
        public async Task RequestQuickPay_Success()
        {
            var request = CreateReqeust(PaymentMethod.QuickPay);

            var response = await new MerchantDepositsGatewayClient().Request(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        public async Task QueryQuickPay_Success()
        {
            await RequestQuickPay_Success();
            var request = CreateQueryReqeust(PaymentMethod.QuickPay);

            var response = await new MerchantDepositsGatewayClient().Query(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        [TestMethod]
        public async Task RequestOnlineToBankCard_Success()
        {
            var request = CreateReqeust(PaymentMethod.OnlineToBankCard);

            var response = await new MerchantDepositsGatewayClient().Request(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        public async Task QueryOnlineToBankCard_Success()
        {
            await RequestOnlineToBankCard_Success();
            var request = CreateQueryReqeust(PaymentMethod.OnlineToBankCard);

            var response = await new MerchantDepositsGatewayClient().Query(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        [TestMethod]
        public async Task RequestJDWallet_Success()
        {
            var request = CreateReqeust(PaymentMethod.JDWallet);

            var response = await new MerchantDepositsGatewayClient().Request(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        public async Task QueryJDWallet_Success()
        {
            await RequestJDWallet_Success();
            var request = CreateQueryReqeust(PaymentMethod.JDWallet);

            var response = await new MerchantDepositsGatewayClient().Query(request);

            Assert.IsTrue(response.IsSuccessStatusCode);

            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            Assert.IsTrue(context.RequestStatus == "1");
        }

        private MerchantDepositRequestContext CreateReqeust(PaymentMethod paymentMethod)
        {
            merchantOrderNumber = $"UT{DateTime.Now.ToString("yyyyMMddhhmmss")}";

            var request = new MerchantDepositRequestContext
            {
                MerchantOrderNumber = merchantOrderNumber,
                MerchantUsername = "rb001",
                Ip = "127.0.0.1",
                PaymentMethod = ((int)paymentMethod).ToString(),
                PaymentPlatform = "2",
                RequestedAmount = "50.00",
                Username = "utUser01",
                CallbackUrl = "http://www.example.com",
                RedirectionUrl = "http://www.example.com",
                BankCode = paymentMethod == PaymentMethod.OnlineBanking ? "ICC" : "",
                CardSerialNumber = paymentMethod == PaymentMethod.PrepaidCard ? "1234567812346123123" : "",
                CardPassword = paymentMethod == PaymentMethod.PrepaidCard ? "1234567812346123123" : "",
                PayerName = paymentMethod == PaymentMethod.OnlineToBankCard ? "测试" : ""
            };
            request.Sign = AesEncrypt(CreateSignRawText(request, string.Empty, string.Empty), "rb123qwe");

            return request;
        }

        private MerchantQueryRequestContext CreateQueryReqeust(PaymentMethod paymentMethod)
        {
            var request = new MerchantQueryRequestContext
            {
                MerchantOrderNumber = merchantOrderNumber,
                MerchantUsername = "rb001",
                PaymentMethod = ((int)paymentMethod).ToString(),
            };
            request.Sign = HttpUtility.UrlEncode(AesEncrypt(CreateSignRawText(request, string.Empty, string.Empty), "rb123qwe"));

            return request;
        }


        #region prod sign

        private static string CreateSignRawText(object content, string keyPropertyName, string merchantKey
             , bool orderBy = true, bool valueExist = true, bool includePropertyName = true, params string[] symbol)
        {
            symbol = symbol.Length != 2 ? new string[] { "=", "&" } : symbol;

            var props = content.GetType().GetProperties()
                .Where(o => !o.GetCustomAttributes(typeof(SignIgnoreAttribute), false).Any()
                && (valueExist ? !string.IsNullOrEmpty(o.GetValue(content)?.ToString()) : !valueExist));
            props = orderBy ? props.OrderBy(o => o.CustomAttributes.Single().ConstructorArguments.Single().Value) : props;

            string[] signArray = props
                .Select(o => $"{(includePropertyName ? o.CustomAttributes.Single().ConstructorArguments.Single().Value : string.Empty)}{symbol[0]}{o.GetValue(content)}").ToArray();

            return string.Join(symbol[1], signArray) + $"{keyPropertyName}{merchantKey}";
        }

        private static string AesEncrypt(string source, string merchantKey)
        {
            string encrypt = "";

            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();

            byte[] key = sha256.ComputeHash(Encoding.UTF8.GetBytes(merchantKey));
            byte[] iv = md5.ComputeHash(Encoding.UTF8.GetBytes(merchantKey));

            aes.Key = key;
            aes.IV = iv;

            byte[] dataByteArray = Encoding.UTF8.GetBytes(source);

            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(dataByteArray, 0, dataByteArray.Length);
                    cs.FlushFinalBlock();
                    encrypt = Convert.ToBase64String(ms.ToArray());
                }
            }

            return encrypt;
        }

        #endregion
    }
}
